# 9key
Live9 encrypted USB keys manager.


## Installation

In your Linux workstation (with USB port):

```
curl -o /tmp/9key https://gitlab.com/live9/bash/9key/-/raw/main/9key
```

```
sudo chown root:root /tmp/9key
```

```
sudo chmod 755 /tmp/9key
```

```
sudo mv /tmp/9key /usr/local/bin/
```

## Setup & Usage

- Requires a pair of USB memories (use known brands)
- Insert your USB memory
- Execute `9key help` and read the EXAMPLES section

## Authors and acknowledgment
Originaly developed in Feb 2020 by Fredy Pulido for Fundacion Karisma.

## License
GNU GPL v2 ++
